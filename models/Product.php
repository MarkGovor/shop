<?php
/**
 * Created by PhpStorm.
 * User: Светлый
 * Date: 02.01.2018
 * Time: 17:22
 */

namespace app\models;


use yii\db\ActiveRecord;

class Product extends ActiveRecord
{
    public function behaviors()
    {

        return[
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];

    }

    public function getCategory(){

        return $this->hasOne(Category::className(), ['id' => 'category_id']);
    }

}