<?php
/**
 * Created by PhpStorm.
 * User: Светлый
 * Date: 02.01.2018
 * Time: 17:18
 */

namespace app\models;


use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
    public function behaviors()
    {

        return[
            'image' => [
                'class' => 'rico\yii2images\behaviors\ImageBehave',
            ]
        ];

    }


    public function getProducts(){


        return $this->hasMany(Product::className(), ['category_id' => 'id']);


    }
}