<?php
/**
 * Created by PhpStorm.
 * User: Светлый
 * Date: 03.01.2018
 * Time: 13:13
 */

namespace app\controllers;
use app\models\Category;
use app\models\Product;
use Codeception\Util\Debug;
use Yii;

use yii\web\Controller;

class AppController extends Controller
{

    protected function setMeta($title = null, $keywords = null, $description = null){

        $this->view->title = $title;
        $this->view->registerMetaTag(['name' => 'keywords', 'content' => "$keywords"]);
        $this->view->registerMetaTag(['name' => 'description', 'content' => "$description"]);


    }


}