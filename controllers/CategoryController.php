<?php
/**
 * Created by PhpStorm.
 * User: Светлый
 * Date: 03.01.2018
 * Time: 13:12
 */

namespace app\controllers;
use app\models\Category;
use app\models\Product;
use Codeception\Util\Debug;
use Yii;
use yii\data\Pagination;

class CategoryController  extends AppController
{


    public function actionIndex(){


        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();

        $this->setMeta('E-Shoper');

      return $this->render('index', compact('hits'));
    }




    public function actionView($id){


        //$id = Yii::$app->request->get('id');
      //  debag($id);
     //   $products = Product::find()->where(['category_id' => $id])->all();

        $category = Category::findOne($id);

        if (empty($category)) {
            throw new \yii\web\HttpException(404, 'Такой категории нет или она была.');
        }

        $query = Product::find()->where(['category_id' => $id]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' =>3, 'forcePageParam' => false, 'pageSizeParam' =>false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();

        $this->setMeta('E-Shoper | ' . $category->name, $category->keywords, $category->description);

        return $this->render('view', compact('products','pages', 'category'));

    }



    public function actionSearch(){


        $q = trim(Yii::$app->request->get('q'));

        $this->setMeta('E-Shoper | Поиск ' . $q);


        if(!$q)
            return $this->render('search');
        //  debag($id);
        //   $products = Product::find()->where(['category_id' => $id])->all();


        $category = Category::findOne($q);


        $query = Product::find()->where(['like', 'name', $q]);
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' =>3, 'forcePageParam' => false, 'pageSizeParam' =>false]);
        $products = $query->offset($pages->offset)->limit($pages->limit)->all();

        $this->setMeta('E-Shoper | ' . $category->name, $category->keywords, $category->description);

        return $this->render('search', compact('products', 'pages', 'q'));

    }


}