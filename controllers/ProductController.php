<?php
/**
 * Created by PhpStorm.
 * User: Светлый
 * Date: 04.01.2018
 * Time: 17:36
 */

namespace app\controllers;



use app\models\Product;
use app\models\Category;
use Yii;

class ProductController extends AppController
{

    public function actionView($id){



     //   $id = Yii::$app->request->get('id');


        $product = Product::findOne($id);

        if (empty($product)) {
            throw new \yii\web\HttpException(404, 'Такой товара нет или она был.');
        }

        $hits = Product::find()->where(['hit' => '1'])->limit(6)->all();
      //  $product = Product::find()->with('category')->where(['id' => $id])->one();

        return $this->render('view', compact('product', 'hits'));
    }


}