<?

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
?>

<div class="container">

    <? if(Yii::$app->session->hasFlash('success')):?>
        <div class="alert alert-success alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <? echo  Yii::$app->session->getFlash('success')?>
        </div>
    <? endif;?>


    <? if(Yii::$app->session->hasFlash('error')):?>

        <div class="alert alert-danger alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <? echo  Yii::$app->session->getFlash('error')?>
        </div>
    <? endif;?>

    <?php

    if(!empty($session['cart'])): ?>

        <div class="table-responsive">

            <table class="table table-hover table-striped">
                <thead>
                <tr>
                    <th>фото</th>
                    <th>наименаование</th>
                    <th>кол-во</th>
                    <th>цена</th>
                    <th>Cумма</th>
                    <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> </th>
                </tr>
                </thead>

                <tbody>

                <?foreach ($session['cart'] as $id => $item): ?>
                    <tr>
                        <td><?=\yii\helpers\Html::img("@web/images/products/{$item['img']}", ['alt' => $item['name'], 'height' => '50px']);?></td>
                        <td><a href="<?= Url::to(['product/view', 'id' => $id]) ?>"><?=$item['name']?></a> </td>
                        <td><?=$item['qty']?></td>
                        <td><?=$item['price']?></td>
                        <td><?=$item['qty'] * $item['price']?></td>
                        <td><span data-id="<?=$id?>" class="glyphicon glyphicon-remove text-danger del-item" aria-hidden="true"></span> </td>

                    </tr>

                <? endforeach;?>

                <tr>

                    <td colspan="5">Итого</td>
                    <td><?= $session['cart.qty']?></td>
                </tr>

                <tr>

                    <td colspan="5">На сумму</td>
                    <td><?= $session['cart.sum']?></td>
                </tr>




                </tbody>


            </table>

        </div>

        <hr/>

        <?php $form = ActiveForm::begin()?>

        <?= $form->field($order, 'name') ?>
        <?= $form->field($order, 'email') ?>
        <?= $form->field($order, 'phone') ?>
        <?= $form->field($order, 'address') ?>
        <?= Html::submitButton('Заказать', ['class' => 'btn btn-success'])?>

        <? ActiveForm::end()?>

       <br>

    <? else:?>

        <h3>Корзина пуста</h3>

    <? endif;?>

</div>
