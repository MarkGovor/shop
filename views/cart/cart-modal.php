<?php

if(!empty($session['cart'])): ?>

    <div class="table-responsive">

        <table class="table table-hover table-striped">
<thead>
<tr>
<th>фото</th>
<th>наименаование</th>
<th>кол-во</th>
<th>цена</th>
    <th><span class="glyphicon glyphicon-remove" aria-hidden="true"></span> </th>
</tr>
</thead>

            <tbody>

            <?foreach ($session['cart'] as $id => $item): ?>
           <tr>
             <td><?=\yii\helpers\Html::img($item['img'], ['alt' => $item['name'], 'height' => '50px']);?></td>
                <td><?=$item['name']?></td>
                <td><?=$item['qty']?></td>
                <td><?=$item['price']?></td>
                <td><span data-id="<?=$id?>" class="glyphicon glyphicon-remove text-danger del-item" aria-hidden="true"></span> </td>

                </tr>

             <? endforeach;?>

            <tr>

                <td colspan="4">Итого</td>
                <td><?= $session['cart.qty']?></td>
            </tr>

            <tr>

                <td colspan="4">На сумму</td>
                <td><?= $session['cart.sum']?></td>
            </tr>




            </tbody>


        </table>

    </div>

<? else:?>

<h3>Корзина пуста</h3>

<? endif;?>
